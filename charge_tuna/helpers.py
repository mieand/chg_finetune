from ase.io import read, write
import numpy as np
import torch
from pymatgen.io.ase import AseAtomsAdaptor
from chgnet.data.dataset import StructureData
from chgnet.model import CHGNet

def check_device():
    if torch.cuda.is_available():
        device = "cuda"
    else:
        device = "cpu"
    return device

def load_datasets(dataset_names):
    if dataset_names is None:
        return []

    datasets = []
    for dataset_name in dataset_names:
        datasets.extend(load_dataset(dataset_name))
    return datasets

def load_dataset(dataset_name):
    if '@' in dataset_name:
        limit = None
    else: 
        limit = ':'
    data = read(dataset_name, limit)
    return data

def shuffle_dataset(data, seed=1):
    np.random.seed(seed)
    np.random.shuffle(data)
    return data

def convert_to_structure(data):
    structures = []
    for atoms in data:
        structures.append(AseAtomsAdaptor.get_structure(atoms))
    return structures

def convert_to_structure_dataset(data, structures):
    # Setup targets:
    energies_per_atom = np.array(
        [atoms.get_potential_energy() / len(atoms) for atoms in data]
    )
    forces = [atoms.get_forces(apply_constraint=False) for atoms in data]

    # Setup dataset & loaders:
    dataset = StructureData(
        structures=structures,
        energies=energies_per_atom,
        forces=forces,
        stresses=None,
        magmoms=None,
    )
    return dataset, energies_per_atom

def evaluate_model(atoms_data, model, batch_size=8):
    structures = convert_to_structure(atoms_data)
    results = []
    for i in range(0, len(structures), batch_size):
        batch = structures[i:i + batch_size]
        result = model.predict_structure(batch, task='ef')
        if isinstance(result, list):
            results += result
        else:
            results.append(result)

    energies = np.array([result['e'] for result in results]) * np.array([len(atoms) for atoms in atoms_data])
    return energies

def load_model(path, device=None):
    if device is None:
        device = check_device()

    model = CHGNet.from_file(path)
    model.to(device)
    return model

def check_constraints(data):
    for atoms in data: 
        constraints = atoms.constraints
        indices = []
        for constraint in constraints:
            indices.extend(constraint.get_indices())

        if len(indices) > 0:
            forces = atoms.get_forces(apply_constraint=False)
            if np.all(forces[indices] == 0):
                return False
    return True 





