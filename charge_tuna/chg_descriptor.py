from agox.models.descriptors import DescriptorBaseClass
from pymatgen.io.ase import AseAtomsAdaptor
import torch
from chgnet.model.model import BatchedGraph

class CHGNetDescriptor(DescriptorBaseClass): 

    name = 'CHGNetDescriptor'
    descriptor_type='global'

    def __init__(self, model, **kwargs):
        super().__init__(environment=None, **kwargs)

        self.model = model

    def create_features(self, atoms):
        structure = AseAtomsAdaptor.get_structure(atoms)
        graph = self.model.graph_converter(structure)

        batched_graph = BatchedGraph.from_graphs(
            [graph],
            bond_basis_expansion=self.model.bond_basis_expansion,
            angle_basis_expansion=self.model.angle_basis_expansion,
            compute_stress=False)

        results = self.model._compute(batched_graph, 
                            return_crystal_feas=True)
        
        crystal_feas = results['crystal_fea'].detach().numpy()
                                
        return crystal_feas
    
    def create_feature_gradient(self, atoms):
        structure = AseAtomsAdaptor.get_structure(atoms)
        graph = self.model.graph_converter(structure)

        batched_graph = BatchedGraph.from_graphs(
            [graph],
            bond_basis_expansion=self.model.bond_basis_expansion,
            angle_basis_expansion=self.model.angle_basis_expansion,
            compute_stress=False)
        
        results = self.model._compute(batched_graph, 
                            return_crystal_feas=True)
                            
        
        F = results['crystal_fea']

        batch=[batched_graph.atom_positions[0] for n in F[0]]
        dF=torch.autograd.grad([x for  x in F[0]], batch,
                create_graph=True)


        dF = np.stack([df.detach().numpy() for df in dF]).reshape(1, 2, 3, 64)

        print(dF.shape)

        exit()

        return dF

    # def create_feature_gradient(self, atoms):
    #     return self.central_difference_gradient(atoms, 1e-5)
    
    def get_number_of_centers(self, atoms):
        return 1
    
    def central_difference_gradient(self, atoms, delta=0.0001):
        """
        Calculate the gradient of the features using a central difference method.
        """

        f0 = self.get_features(atoms)
        dF_dx = np.zeros((f0.shape[0], len(atoms), 3, f0.shape[1]))
        
        for i in range(len(atoms)):
            for d in range(3):
                atoms[i].position[d] -= delta
                fm = self.get_features(atoms)
                atoms[i].position[d] += 2 * delta
                fp = self.get_features(atoms)
                atoms[i].position[d] -= delta
                dF_dx[:, i, d, :] = (fp - fm) / (2 * delta)

        return dF_dx
    
if __name__ == "__main__":

    from chgnet.model import CHGNet
    import numpy as np
    from ase import Atoms
    from ase.build import molecule

    model = CHGNet.load()
    descriptor = CHGNetDescriptor(model)

    atoms = molecule('CO')
    atoms.set_cell(np.eye(3)*10)
    atoms.center()

    features = descriptor.get_features(atoms.copy())
    autograd = descriptor.get_feature_gradient(atoms.copy()) #.flatten()

    central_diff = descriptor.central_difference_gradient(atoms, 1e-3) #.flatten()

    D = np.sum(np.abs(central_diff - autograd))

    print(D)

    print(autograd[0, 0, :, 0])

    print(central_diff[0, 0, :, 0])




