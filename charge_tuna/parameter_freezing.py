def freeze_parameters(model, freeze_level=1):
    """
    Freeze the parameters of a model up to a certain level.
    """
    # Freeze the parameters:
    if freeze_level == 0:
        freeze_level_zero(model)
    if freeze_level == 1:
        freeze_level_one(model)

    check_parameters(model)

def freeze_level_one(model):
    for layer in [
        model.atom_embedding,
        model.bond_embedding,
        model.angle_embedding,
        model.bond_basis_expansion,
        model.angle_basis_expansion,
        model.atom_conv_layers[:-1],
        model.bond_conv_layers,
        model.angle_layers,
    ]:
        for param in layer.parameters():
            param.requires_grad = False

    return model

def freeze_level_zero(model):
    return model

def check_parameters(model):

    total_count = 0
    frozen_count = 0
    for name, parameter in model.named_parameters():
        total_count += parameter.numel()
        if not parameter.requires_grad:
            frozen_count += parameter.numel()

    print('')    
    print('Parameter freezing:')
    print(f"    Total parameters: {total_count}")
    print(f"    Frozen parameters: {frozen_count}")
    print(f"    Free parameters: {total_count - frozen_count}")

    
