from chgnet.trainer import Trainer
from chgnet.data.dataset import get_train_val_test_loader, get_loader

from charge_tuna.helpers import check_device, convert_to_structure, convert_to_structure_dataset
from charge_tuna.composition_model import fit_composition_model, optimize_composition_model
from charge_tuna.trainer import CustomTrainer
from charge_tuna.experimental.trainer import CustomTrainer as CustomTrainerExperimental

import torch
import numpy as np

def get_default_trainer_parameters():
    default_trainer_parameters = {
        'targets': 'ef',
        'optimizer': 'Adam',
        'scheduler': 'CosLR',
        'criterion': 'MSE',
        'epochs': 10,
        'learning_rate': 1e-2,            
        'print_freq': 10,
            }
    return default_trainer_parameters

def get_default_data_parameters():
    default_data_parameters = {
        'batch_size': 8,
        'train_ratio': 1.0,
        'val_ratio': 0.00,
        'return_test': True,
            }
    return default_data_parameters

class Tuna:

    def __init__(self, model, device=None):
        if device is None:
            self.device = check_device()
        else:
            self.device = device

        self.model = model

    def train_composition_model(self, data, experimental_comp=False):
        """
        Fit the composition model of the CHGNet model to the current data

        A little dumb that this calls a function from a seperate file maybe. 
        Was easier to get started tho.
        """
        structures = convert_to_structure(data)
        if not experimental_comp:
            fit_composition_model(self.model, data, structures, verbose=True)
        else:
            optimize_composition_model(self.model, data, structures, verbose=True)

    def train_model(self, training_set, validation_set=None, test_set=None, 
                    trainer_parameters=None, data_parameters=None, 
                    save_dir=None, train_composition_model: bool = False) -> dict:
        """
        Arguments:
        trainer_parameters: dict
            Dictionary of parameters for the Trainer class
            keys: 
                targets: str
                optimizer: str
                scheduler: str
                criterion: str
                epochs: int
                learning_rate: float
                print_freq: int
        data_parameters: dict
            Dictionary of parameters for the get_train_val_test_loader function
            keys:
                batch_size: int
                train_ratio: float
                val_ratio: float
        """

        loaders = self.get_data_loaders(training_set=training_set,
                                        validation_set=validation_set, 
                                        test_set=test_set, 
                                        data_parameters=data_parameters)
        
        for loader in loaders:
            print(loader)
        
        trainer = self.get_trainer(trainer_parameters)
        trainer.train(*loaders, save_dir=save_dir, train_composition_model=train_composition_model)

        # history = {}
        # history['e_train'] = trainer.training_history['e']['train']
        # history['e_val'] = trainer.training_history['e']['val']
        # history['f_train'] = trainer.training_history['f']['train']
        # history['f_val'] = trainer.training_history['f']['val']
        # history['learning_rate'] = trainer.training_history['learning_rate']

        return trainer.training_history

    def save_model(self, path):
        model_dict = self.model.as_dict()
        torch.save(model_dict, f"{path}.pt")
        return 

    def get_data_loaders(self, training_set, validation_set=None, test_set=None, data_parameters=None):
        if data_parameters is None:
            data_parameters = {}

        base_data_parameters = get_default_data_parameters()
        base_data_parameters.update(data_parameters)        

        # # No test or validation set, so parameters decide if the training_set is split into the three. 
        # if test_set is None and validation_set is None:
        #     structures = convert_to_structure(training_set)
        #     dataset, _ = convert_to_structure_dataset(training_set, structures)
        #     train_loader, val_loader, test_loader = get_train_val_test_loader(dataset, **base_data_parameters)

        # # Have a validation set:
        # if validation_set is not None:
        #     base_data_parameters['val_ratio'] = 0.0
        
        # structures = convert_to_structure(training_set)
        # dataset, _ = convert_to_structure_dataset(training_set, structures)
        # train_loader, val_loader, test_loader = get_train_val_test_loader(dataset, **base_data_parameters)

        # if validation_set is not None:
        #     structures = convert_to_structure(validation_set)
        #     val_dataset, _ = convert_to_structure_dataset(validation_set, structures)
        #     val_loader = get_loader(val_dataset, batch_size=base_data_parameters['batch_size'])

        # if test_set is not None:
        #     structures = convert_to_structure(test_set)
        #     test_dataset, _ = convert_to_structure_dataset(test_set, structures)
        #     test_loader = get_loader(test_dataset, batch_size=base_data_parameters['batch_size'])

        loaders = []
        for data_set in [training_set, validation_set, test_set]:
            if data_set is not None:
                structures = convert_to_structure(data_set)
                dataset, _ = convert_to_structure_dataset(data_set, structures)
                loader = get_loader(dataset, batch_size=base_data_parameters['batch_size'])
                loaders.append(loader)

        return loaders[0], loaders[1], loaders[2]

    def get_trainer(self, trainer_parameters=None):

        if trainer_parameters is None:
            trainer_parameters = {}

        base_trainer_parameters = get_default_trainer_parameters()

        base_trainer_parameters.update(trainer_parameters)
        trainer = CustomTrainerExperimental(model=self.model, **base_trainer_parameters)

        return trainer
    
    def evaluate_model(self, data):
        """
        Evaluate the model on the given data
        """

        true_energies = np.array([atoms.get_potential_energy() for atoms in data])
        true_forces = np.concatenate([atoms.get_forces(apply_constraint=False).flatten() for atoms in data])
        n_atoms = np.array([len(atoms) for atoms in data])

        structures = convert_to_structure(data)

        predictions = self.model.predict_structure(structures)
        predicted_energies = np.array([p['e'] for p in predictions]) * n_atoms
        predicted_forces = np.concatenate([p['f'].flatten() for p in predictions])

        mae = np.mean(np.abs(true_energies - predicted_energies))
        rmse = np.sqrt(np.mean((true_energies - predicted_energies)**2))

        rmse_per_atom = np.sqrt(np.mean(((true_energies - predicted_energies)/n_atoms)**2))
        mae_per_atom = np.mean(np.abs((true_energies - predicted_energies)/n_atoms))

        force_mae = np.mean(np.abs(true_forces - predicted_forces))
        force_rmse = np.sqrt(np.mean((true_forces - predicted_forces)**2))

        return {'MAE': mae, 
                'RMSE': rmse, 
                'MAE_per_atom': mae_per_atom,
                'RMSE_per_atom': rmse_per_atom,
                'predicted_energies': predicted_energies, 
                'true_energies': true_energies, 
                'predicted_forces': predicted_forces,
                'true_forces': true_forces,          
                'n_atoms': n_atoms,
                'Force MAE': force_mae,
                'Force RMSE': force_rmse
                }

