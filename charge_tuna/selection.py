import numpy as np

def uniform_energy_selection(data, energy_step):
    energies = np.array([atoms.get_potential_energy() for atoms in data])

    # Sort according to energies: 
    indices = np.argsort(energies)
    data = [data[i] for i in indices]
    energies = energies[indices]

    # Select with energy_step eV intervals: 
    selected = []
    selected_energies = []
    for i, (energy, atoms) in enumerate(zip(energies, data)):

        if len(selected) == 0:
            selected.append(atoms)
            selected_energies.append(energy)
            continue

        elif energy - selected_energies[-1] > energy_step:
            selected.append(atoms)
            selected_energies.append(energy)

    return selected

def kmeans_selection(data, n_clusters):
    from sklearn.cluster import KMeans

    # Get the features
    F = [atoms.info['crystal_fea'] for atoms in data]

    labels = KMeans(n_clusters=n_clusters).fit_predict(F)
    unique_labels = set(labels)
    selected = []

    energies = np.array([atoms.get_potential_energy() for atoms in data])

    for label in unique_labels:
        indices = np.argwhere(labels == label).flatten()
        min_index = np.argmin(energies[indices])
        selected.append(data[indices[min_index]])

    return selected

def farthest_point_sampling(distances):
    N = distances.shape[0]
    farthest_indices = np.zeros(N, int)
    ds = distances[0, :]
    for i in range(1, N):
        idx = np.argmax(ds)
        farthest_indices[i] = idx
        ds = np.minimum(ds, distances[idx, :])
    return farthest_indices
    
def fps_selection(data, n_samples):
    """
        Farthest point sampling on global features
    """
    from scipy.spatial.distance import cdist

    features = [atoms.info['crystal_fea'] for atoms in data]
    distances = cdist(features, features)
    selected_indices = farthest_point_sampling(distances)[:n_samples]
    selected = [data[idx] for idx in selected_indices]
    
    return selected