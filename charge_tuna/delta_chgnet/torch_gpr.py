import torch

class TorchGPR:

    def __init__(self, kernel, noise=1e-2):
        self.kernel = kernel
        self.noise = noise

    def fit(self, X, y):
        
        self.mean = torch.mean(y)

        self.X = X
        self.y = y - self.mean
        self.K = self.kernel(X, X)
        self.K += self.noise * torch.eye(X.shape[0])
        self.L = torch.linalg.cholesky(self.K)
        self.alpha = torch.cholesky_solve(self.y, self.L)

        self.K_inv = torch.cholesky_inverse(self.L)

    def predict(self, X):
        K_star = self.kernel(X, self.X)
        f_mean = K_star @ self.alpha + self.mean

        # Variance
        k0 = self.kernel(X, X)

        var = torch.diag(k0 - torch.einsum('ij,ji->i', K_star @ self.K_inv, K_star.T))

        return f_mean, torch.sqrt(var)
