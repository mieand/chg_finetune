from charge_tuna import CHGNetModel
from .delta_chg import DeltaCHGNet

class DeltaCHGNetModel(CHGNetModel):

    def __init__(self, model: DeltaCHGNet, gpr_parameters: dict | None = None, **kwargs):

        # if not isinstance(model, DeltaCHGNet):
        #     raise ValueError(f"{model=} must be a DeltaCHGNet")
        super().__init__(model, synchronize=True, **kwargs)
        if gpr_parameters is None:
            gpr_parameters = {}
        self.gpr_parameters = gpr_parameters

    def train(self, data, *args, **kwargs):
        self.writer('TRAINING')
        self.model.train_gpr(data, **self.gpr_parameters)

    def predict_all(self, atoms, task='ef', atomic_features=False, crystal_features=True, **kwargs):
        model_prediction = self.model.predict_atoms(atoms)

        if self.is_intensive:
            factor = len(atoms)
        else:
            factor = 1

        model_prediction['e'] = model_prediction['e']*factor

        results = {}
        if 'e' in task:
            results['energy'] = model_prediction['e']
        if 'f' in task:
            results['forces'] = model_prediction['f']        
        if atomic_features:
            results['atomic_features'] = model_prediction['atom_fea']    

        if crystal_features:
            results['crystal_features'] = model_prediction['crystal_fea']
        
        if 'std' in model_prediction:
            results['uncertainty'] = model_prediction['std']

        if 'f_std' in model_prediction:            
            results['uncertainty_forces'] = model_prediction['f_std']

        atoms.info.update(model_prediction)
            
        return results
    
    def predict_uncertainty(self, atoms, uncertainty=None, **kwargs):
        if uncertainty is None:
            return self.predict_all(atoms, task='ef', **kwargs)['uncertainty']
        else:
            return uncertainty

    def predict_uncertainty_forces(self, atoms, uncertainty_forces=None, **kwargs):
        if uncertainty_forces is None:
            return self.predict_all(atoms, task='ef', **kwargs)['uncertainty_forces']
        else:
            return uncertainty_forces
