from __future__ import annotations

import datetime
import inspect
import os
import random
import shutil
import time
from typing import TYPE_CHECKING, Literal

import numpy as np
import torch
from torch import Tensor, nn
from torch.optim.lr_scheduler import (
    ReduceLROnPlateau,
    ConstantLR
)

from chgnet.model.model import CHGNet
from chgnet.utils import AverageMeter, cuda_devices_sorted_by_free_mem, mae, write_json

if TYPE_CHECKING:
    from torch.utils.data import DataLoader

    from chgnet import TrainTask


class CustomTrainer:
    """
    Customized version of the CHGNet Trainer class (chgnet.trainer.Trainer).

    A trainer to train CHGNet using energy, force, stress and magmom.    
    """

    def __init__(
        self,
        model: nn.Module | None = None,
        targets: TrainTask = "ef",
        energy_loss_ratio: float = 1,
        force_loss_ratio: float = 1,
        stress_loss_ratio: float = 0.1,
        mag_loss_ratio: float = 0.1,
        optimizer: str = "Adam",
        scheduler: str = "ReduceLROnPlateau",
        criterion: str = "MSE",
        epochs: int = 50,
        starting_epoch: int = 0,
        learning_rate: float = 1e-3,
        print_freq: int = 100,
        torch_seed: int | None = None,
        data_seed: int | None = None,
        use_device: str | None = None,
        weight_function: callable | None = None,
        **kwargs,
    ) -> None:
        """Initialize all hyper-parameters for trainer.

        Args:
            model (nn.Module): a CHGNet model
            targets ("ef" | "efs" | "efsm"): The training targets. Default = "ef"
            energy_loss_ratio (float): energy loss ratio in loss function
                Default = 1
            force_loss_ratio (float): force loss ratio in loss function
                Default = 1
            stress_loss_ratio (float): stress loss ratio in loss function
                Default = 0.1
            mag_loss_ratio (float): magmom loss ratio in loss function
                Default = 0.1
            optimizer (str): optimizer to update model. Can be "Adam", "SGD", "AdamW",
                "RAdam". Default = 'Adam'
            scheduler (str): learning rate scheduler.
            criterion (str): loss function criterion. Can be "MSE", "Huber", "MAE"
                Default = 'MSE'
            epochs (int): number of epochs for training
                Default = 50
            starting_epoch (int): The epoch number to start training at.
            learning_rate (float): initial learning rate
                Default = 1e-3
            print_freq (int): frequency to print training output
                Default = 100
            torch_seed (int): random seed for torch
                Default = None
            data_seed (int): random seed for random
                Default = None
            use_device (str, optional): device name to train the CHGNet.
                Can be "cuda", "cpu"
                Default = None
            **kwargs (dict): additional hyper-params for optimizer, scheduler, etc.
        """
        # Store trainer args for reproducibility
        self.trainer_args = {
            k: v
            for k, v in locals().items()
            if k not in ["self", "__class__", "model", "kwargs"]
        }
        self.trainer_args.update(kwargs)

        self.model = model
        self.targets = targets

        if torch_seed is not None:
            torch.manual_seed(torch_seed)
        if data_seed:
            random.seed(data_seed)

        self.optimizer = self.get_optimizer(optimizer, learning_rate, **kwargs)
        self.scheduler = self.get_schedular(scheduler, epochs, learning_rate, **kwargs)
        
        # Define loss criterion
        self.criterion = CombinedLoss(
            target_str=self.targets,
            criterion=criterion,
            is_intensive=self.model.is_intensive,
            energy_loss_ratio=energy_loss_ratio,
            force_loss_ratio=force_loss_ratio,
            stress_loss_ratio=stress_loss_ratio,
            mag_loss_ratio=mag_loss_ratio,
            weight_function=weight_function,
            **kwargs,
        )
        self.epochs = epochs
        self.starting_epoch = starting_epoch

        # Determine the device to use
        if use_device is not None:
            self.device = use_device
        elif torch.cuda.is_available():
            self.device = "cuda"
        else:
            self.device = "cpu"
        if self.device == "cuda":
            # Determine cuda device with most available memory
            device_with_most_available_memory = cuda_devices_sorted_by_free_mem()[0]
            self.device = f"cuda:{device_with_most_available_memory}"

        self.print_freq = print_freq
        # self.training_history: dict[
        #     str, dict[Literal["train", "val", "test"], list[float]]
        # ] = {key: {"train": [], "val": [], "test": []} for key in self.targets}
        self.training_history = {}
        self.best_model = None

    def get_schedular(self, scheduler, epochs, learning_rate, **kwargs):
        # Define learning rate scheduler

        if scheduler == 'ReduceLROnPlateau':
            scheduler = ReduceLROnPlateau(self.optimizer, mode='min', factor=0.5, 
                                                                   patience=10, 
                                                                   verbose=True, 
                                                                   threshold=1e-4, 
                                                                   threshold_mode='rel', 
                                                                   cooldown=0, 
                                                                   min_lr=0, 
                                                                   eps=1e-8)
            self.scheduler_type = 'plateau'
        elif scheduler == 'Constant':
            scheduler = ConstantLR(self.optimizer, factor=1)
            self.scheduler_type = 'constant'
        else:
            print(f"Scheduler {scheduler} not recognized")
            raise NotImplementedError
        
        return scheduler

    def get_optimizer(self, optimizer, learning_rate, **kwargs):
        # Define optimizer
        if optimizer == "SGD":
            momentum = kwargs.pop("momentum", 0.9)
            weight_decay = kwargs.pop("weight_decay", 0)
            optimizer = torch.optim.SGD(
                self.model.parameters(),
                learning_rate,
                momentum=momentum,
                weight_decay=weight_decay,
            )
        elif optimizer == "Adam":
            weight_decay = kwargs.pop("weight_decay", 0)
            optimizer = torch.optim.Adam(
                self.model.parameters(), learning_rate, weight_decay=weight_decay
            )
        elif optimizer == "AdamW":
            weight_decay = kwargs.pop("weight_decay", 1e-2)
            optimizer = torch.optim.AdamW(
                self.model.parameters(), learning_rate, weight_decay=weight_decay
            )
        elif optimizer == "RAdam":
            weight_decay = kwargs.pop("weight_decay", 0)
            optimizer = torch.optim.RAdam(
                self.model.parameters(), learning_rate, weight_decay=weight_decay
            )
            
        return optimizer

    def train(
        self,
        train_loader: DataLoader,
        val_loader: DataLoader,
        test_loader: DataLoader | None = None,
        save_dir: str | None = None,
        save_test_result: bool = False,
        train_composition_model: bool = False,
    ) -> None:
        """Train the model using torch data_loaders.

        Args:
            train_loader (DataLoader): train loader to update CHGNet weights
            val_loader (DataLoader): val loader to test accuracy after each epoch
            test_loader (DataLoader):  test loader to test accuracy at end of training.
                Can be None.
                Default = None
            save_dir (str): the dir name to save the trained weights
                Default = None
            save_test_result (bool): Whether to save the test set prediction in a JSON
                file. Default = False
            train_composition_model (bool): whether to train the composition model
                (AtomRef), this is suggested when the fine-tuning dataset has large
                elemental energy shift from the pretrained CHGNet, which typically comes
                from different DFT pseudo-potentials.
                Default = False
        """
        if self.model is None:
            raise ValueError("Model needs to be initialized")
        global best_checkpoint  # noqa: PLW0603

        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        print(f"Begin Training: using {self.device} device")
        print(f"training targets: {self.targets}")
        self.model.to(self.device)

        # Turn composition model training on / off
        for param in self.model.composition_model.parameters():
            param.requires_grad = train_composition_model

        self.report(None, None, None, header=True)

        for epoch in range(self.starting_epoch, self.epochs):
            
            # Training:
            losses = self._train(train_loader, epoch)

            # Validation:
            validation = self._validate(val_loader)

            # Learning rate scheduler:
            if self.scheduler_type == 'plateau':
                self.scheduler.step(validation['validation_loss'])
            else:
                self.scheduler.step()

            # Book-keeeping:
            self.record_training_history(epoch, losses, validation)
            self.save_checkpoint(epoch, validation, save_dir=save_dir)
            self.report(epoch, losses, validation)


        np.savez(f'{save_dir}/training_stats.npz', **self.training_history)
        self.save(f'{save_dir}/final_model.tar')

    def record_training_history(self, epoch, losses, validation):            
            if len(self.training_history) == 0:
                for key, value in losses.items():
                    self.training_history[key] = np.ones(self.epochs) * np.inf
                for key, value in validation.items():
                    self.training_history[key] = np.ones(self.epochs) * np.inf

            for key, value in losses.items():
                self.training_history[key][epoch] = value
            for key, value in validation.items():
                self.training_history[key][epoch] = value


    def report(self, epoch, losses, validation, header=False):
        n_chars=10

        if header:
            header_types = [None, None, 'Training', 'Training', 'Training', 'Validation', 'Validation', 'Validation']
            terms = ['Epoch', 'Time', 'Loss', 'E MAE', 'F MAE', 'Loss', 'E MAE', 'F MAE']
            header = ''

            none_length=0
            training_length=0
            validation_length=0

            previous_htype=None
            for term, htype in zip(terms, header_types):
                if htype is None:

                    if term == "Time":
                        none_length += n_chars + 7
                    else:
                        none_length += n_chars + 1
                elif htype == 'Training':
                    training_length += n_chars + 1
                elif htype == 'Validation':
                    validation_length += n_chars + 1

                if htype != previous_htype:
                    header += '|'
                previous_htype = htype
                
                if term == 'Time':
                    header += f'|{term:^{n_chars+6}s}'
                else:
                    header += f'|{term:^{n_chars}s}'

            none_block = '|'+f'{"General":^{none_length-1}s}' + '|'
            training_block = f'|{"Training":^{training_length-1}s}|'
            validation_block = f'|{"Validation":^{validation_length-1}s}|'

            print('_' * len(header))
            print(none_block + training_block + validation_block)
            print(header + '|')
            print("-" * len(header))
            return 
        else:        
            terms = ['|', losses['loss'], losses['e_MAE'], 
                     losses['f_MAE'], '|', validation['validation_loss'], validation['validation_e_MAE'], 
                     validation['validation_f_MAE']]
            row = f"|{epoch+1:^{n_chars}d}"
            key = "time"
            row += f"|{f'{losses[key]:.2f}' + f' + {validation[key]:.2f}':^{n_chars+6}s}"
            for term in terms:
                if term == '|':
                    row += '|'
                else:
                    row += f'|{term:^{n_chars}.3f}'
            print(row + '|')

    def _train(self, train_loader: DataLoader, current_epoch: int) -> dict:
        """Train all data for one epoch.

        Args:
            train_loader (DataLoader): train loader to update CHGNet weights
            current_epoch (int): used for resume unfinished training

        Returns:
            dictionary of training errors
        """

        # switch to train mode
        self.model.train()

        losses = {key:0.0 for key in ['e_MAE', 'f_MAE', 'loss']}

        t0 = time.perf_counter()
        for idx, (graphs, targets) in enumerate(train_loader):
            # get input
            for g in graphs:
                requires_force = "f" in self.targets
                g.atom_frac_coord.requires_grad = requires_force
            graphs = [g.to(self.device) for g in graphs]
            targets = {k: self.move_to(v, self.device) for k, v in targets.items()}

            # Compute the model predictions:
            prediction = self.model(graphs, task=self.targets)

            # Compute the loss:
            combined_loss = self.criterion(targets, prediction)

            # compute gradient and do SGD step
            self.optimizer.zero_grad()
            combined_loss["loss"].backward()
            self.optimizer.step()

            for key in losses:
                losses[key] += combined_loss[key].item()

        losses = {k: v / len(train_loader) for k, v in losses.items()}
        losses['time'] = time.perf_counter() - t0

        return losses

    def _validate(
        self,
        val_loader: DataLoader,
    ) -> dict:
        """Validation or test step.

        Args:
            val_loader (DataLoader): val loader to test accuracy after each epoch
            is_test (bool): whether it's test step
            test_result_save_path (str): path to save test_result

        Returns:
            dictionary of training errors
        """
        losses = {key: 0.0 for key in ['validation_e_MAE', 'validation_f_MAE', 'validation_loss']}
        # switch to evaluate mode
        self.model.eval()

        t0 = time.perf_counter()
        for idx, (graphs, targets) in enumerate(val_loader):
            if "f" in self.targets or "s" in self.targets:
                for g in graphs:
                    requires_force = "f" in self.targets
                    g.atom_frac_coord.requires_grad = requires_force
                graphs = [g.to(self.device) for g in graphs]
                targets = {k: self.move_to(v, self.device) for k, v in targets.items()}
            else:
                with torch.no_grad():
                    graphs = [g.to(self.device) for g in graphs]
                    targets = {
                        k: self.move_to(v, self.device) for k, v in targets.items()
                    }

            # compute output
            prediction = self.model(graphs, task=self.targets)
            combined_loss = self.criterion(targets, prediction)

            losses['validation_loss'] += combined_loss["loss"].data.cpu().item()
            for key in self.targets:
                losses[f"validation_{key}_MAE"] += combined_loss[f"{key}_MAE"].data.cpu().item()

        losses = {k: v / len(val_loader) for k, v in losses.items()}
        losses['time'] = time.perf_counter() - t0
        return losses

    def get_best_model(self):
        """Get best model recorded in the trainer."""
        if self.best_model is None:
            raise RuntimeError("the model needs to be trained first")
        MAE = min(self.training_history["e"]["val"])
        print(f"Best model has val {MAE =:.4}")
        return self.best_model

    @property
    def _init_keys(self):
        return [
            key
            for key in list(inspect.signature(Trainer.__init__).parameters)
            if key not in (["self", "model", "kwargs"])
        ]

    def save(self, filename: str = "training_result.pth.tar") -> None:
        """Save the model, graph_converter, etc."""
        state = {
            "model": self.model.as_dict(),
            "optimizer": self.optimizer.state_dict(),
            "scheduler": self.scheduler.state_dict(),
            "training_history": self.training_history,
            "trainer_args": self.trainer_args,
        }
        torch.save(state, filename)

    def save_checkpoint(
        self, epoch: int, mae_error: dict, save_dir: str | None = None
    ) -> None:
        """Function to save CHGNet trained weights after each epoch.

        Args:
            epoch (int): the epoch number
            mae_error (dict): dictionary that stores the MAEs
            save_dir (str): the directory to save trained weights
        """
        for fname in os.listdir(save_dir):
            if fname.startswith("epoch"):
                os.remove(os.path.join(save_dir, fname))

        filename = os.path.join(save_dir, f"epoch{epoch}.pth.tar")
        self.save(filename=filename)

        # save the model if it has minimal val energy error or val force error
        if mae_error["validation_e_MAE"] == min(self.training_history["validation_e_MAE"]):
            self.best_model = self.model
            for fname in os.listdir(save_dir):
                if fname.startswith("bestE"):
                    os.remove(os.path.join(save_dir, fname))
            shutil.copyfile(
                filename,
                os.path.join(save_dir, f"bestE_epoch{epoch}.pth.tar"),
            )
        if mae_error["validation_f_MAE"] == min(self.training_history["validation_f_MAE"]):
            for fname in os.listdir(save_dir):
                if fname.startswith("bestF"):
                    os.remove(os.path.join(save_dir, fname))
            shutil.copyfile(
                filename,
                os.path.join(save_dir, f"bestF_epoch{epoch}.pth.tar"),
            )
        if mae_error["validation_loss"] == min(self.training_history["validation_loss"]):
            for fname in os.listdir(save_dir):
                if fname.startswith("bestL"):
                    os.remove(os.path.join(save_dir, fname))
            shutil.copyfile(
                filename,
                os.path.join(save_dir, f"bestL_epoch{epoch}.pth.tar"),
            )


    @classmethod
    def load(cls, path: str) -> CustomTrainer:
        """Load trainer state_dict."""
        state = torch.load(path, map_location=torch.device("cpu"))
        model = CHGNet.from_dict(state["model"])
        print(f"Loaded model params = {sum(p.numel() for p in model.parameters()):,}")
        # drop model from trainer_args if present
        state["trainer_args"].pop("model", None)
        trainer = Trainer(model=model, **state["trainer_args"])
        trainer.model.to(trainer.device)
        trainer.optimizer.load_state_dict(state["optimizer"])
        trainer.scheduler.load_state_dict(state["scheduler"])
        trainer.training_history = state["training_history"]
        trainer.starting_epoch = len(trainer.training_history["e"]["train"])
        return trainer

    @staticmethod
    def move_to(obj, device) -> Tensor | list[Tensor]:
        """Move object to device."""
        if torch.is_tensor(obj):
            return obj.to(device)
        if isinstance(obj, list):
            out = []
            for tensor in obj:
                if tensor is not None:
                    out.append(tensor.to(device))
                else:
                    out.append(None)
            return out
        raise TypeError("Invalid type for move_to")


class CombinedLoss(nn.Module):
    """A combined loss function of energy, force, stress and magmom."""

    def __init__(
        self,
        target_str: str = "ef",
        criterion: str = "MSE",
        is_intensive: bool = True,
        energy_loss_ratio: float = 1,
        force_loss_ratio: float = 1,
        stress_loss_ratio: float = 0.1,
        mag_loss_ratio: float = 0.1,
        delta: float = 0.1,
        weight_function = None,
    ) -> None:
        """Initialize the combined loss.

        Args:
            target_str: the training target label. Can be "e", "ef", "efs", "efsm" etc.
                Default = "ef"
            criterion: loss criterion to use
                Default = "MSE"
            is_intensive (bool): whether the energy label is intensive
                Default = True
            energy_loss_ratio (float): energy loss ratio in loss function
                Default = 1
            force_loss_ratio (float): force loss ratio in loss function
                Default = 1
            stress_loss_ratio (float): stress loss ratio in loss function
                Default = 0.1
            mag_loss_ratio (float): magmom loss ratio in loss function
                Default = 0.1
            delta (float): delta for torch.nn.HuberLoss. Default = 0.1
        """
        super().__init__()
        # Define loss criterion
        if criterion in ["MSE", "mse"]:
            self.criterion = nn.MSELoss(reduce=False)
        elif criterion in ["MAE", "mae", "l1"]:
            self.criterion = nn.L1Loss(reduce=False)
        elif criterion == "Huber":
            self.criterion = nn.HuberLoss(delta=delta, reduce=False)
        else:
            raise NotImplementedError
        self.target_str = target_str
        self.is_intensive = is_intensive
        self.energy_loss_ratio = energy_loss_ratio
        if "f" not in self.target_str:
            self.force_loss_ratio = 0
        else:
            self.force_loss_ratio = force_loss_ratio
        if "s" not in self.target_str:
            self.stress_loss_ratio = 0
        else:
            self.stress_loss_ratio = stress_loss_ratio
        if "m" not in self.target_str:
            self.mag_loss_ratio = 0
        else:
            self.mag_loss_ratio = mag_loss_ratio

        # Weight function:
        if weight_function is None:
            self.weight_function = lambda x: torch.ones_like(x)
        else:
            self.weight_function = weight_function

    def forward(
        self,
        targets: dict[str, Tensor],
        prediction: dict[str, Tensor],
    ) -> dict[str, Tensor]:
        """Compute the combined loss using CHGNet prediction and labels
        this function can automatically mask out magmom loss contribution of
        data points without magmom labels.

        Args:
            targets (dict): DFT labels
            prediction (dict): CHGNet prediction

        Returns:
            dictionary of all the loss, MAE and MAE_size
        """
        out = {"loss": 0.0}
        # Energy

        weights = self.weight_function(targets['e'])

        if self.energy_loss_ratio != 0 and "e" in targets:
            if self.is_intensive:
                out["loss"] += self.energy_loss_ratio * torch.mean(weights * self.criterion(targets["e"], prediction["e"]))
                out["e_MAE"] = mae(targets["e"], prediction["e"])
                out["e_MAE_size"] = prediction["e"].shape[0]
            else:
                e_per_atom_target = targets["e"] / prediction["atoms_per_graph"]
                e_per_atom_pred = prediction["e"] / prediction["atoms_per_graph"]
                out["loss"] += self.criterion(e_per_atom_target, e_per_atom_pred)
                out["e_MAE"] = mae(e_per_atom_target, e_per_atom_pred)
                out["e_MAE_size"] = prediction["e"].shape[0]
        else:
            out["e_MAE"] = torch.zeros([1])
            out["e_MAE_size"] = prediction["e"].shape[0]

        # Force
        if self.force_loss_ratio != 0 and "f" in targets:
            forces_pred = torch.cat(prediction["f"], dim=0)
            forces_target = torch.cat(targets["f"], dim=0)

            force_weight = torch.unsqueeze(torch.repeat_interleave(weights, prediction['atoms_per_graph'], dim=0), 1)

            out["loss"] += self.force_loss_ratio * torch.mean(force_weight * self.criterion(
                forces_target, forces_pred
            ))
            out["f_MAE"] = mae(forces_target, forces_pred)
            out["f_MAE_size"] = forces_target.shape[0]

        return out
