import configparser
import inspect
import os
from pathlib import Path

class Config:

    def __init__(self, config_path):
        self.parser = configparser.ConfigParser()
        self.parser.read(config_path)
    
def get_default_config_path():
    path = Path(os.path.abspath(__file__))
    return path.parent.parent / 'config.ini'

get_default_config_path()

config_path = os.environ.get('TUNA_CONFIG_PATH')
if config_path is None:
    config_path = get_default_config_path()

cfg = Config(config_path)