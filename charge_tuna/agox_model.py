from typing import List

import numpy as np
from ase import Atoms
from ase.calculators.calculator import all_changes, all_properties, Calculator

from agox.models import ModelBaseClass

from chgnet.model import CHGNet
from pymatgen.io.ase import AseAtomsAdaptor

from charge_tuna.helpers import check_device
from charge_tuna.tune import Tuna
import torch

from pathlib import Path

from tqdm import tqdm


class CHGNetModel(ModelBaseClass):
    name = "CHGNetAGOXModel"
    implemented_properties = ["energy", "forces", "uncertainty", "uncertainty_forces"]
    dynamic_attributes = ["model"]  #  Simplest way I have found so far.

    def __init__(
        self,
        model: CHGNet | Path | None,
        predict_device="cpu",
        train_device=None,
        learning_rate=1e-4,
        epochs=10,
        train_composition_model=False,
        batch_size=8,
        synchronize=True,
        is_intensive=True,
        **kwargs,
    ):
        super().__init__(**kwargs)

        self.predict_device = predict_device
        self.train_device = (
            train_device or check_device()
        )  # Prioritizes GPU if available, unless explicitly set to CPU.

        if model is None:
            model = CHGNet.load()
        elif isinstance(model, Path) or isinstance(model, str):
            model = CHGNet.from_file(model)
        elif isinstance(model, torch.nn.Module):
            model = model
        else:
            raise ValueError("Model must be a CHGNet or DeltaCHGNet object or a path to a model file.")

        self.model = model
        self.model.to(self.predict_device)

        # Set intensive-ness:
        self.is_intensive = is_intensive
        self.set_intensive(is_intensive)


        self.ready_state = True

        # Training parameters:
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.train_composition_model = train_composition_model
        self.batch_size = batch_size

        # Synchronization / Ray:
        self.synchronize = synchronize
        if not self.synchronize:
            self.self_synchronizing = True


    def set_intensive(self, is_intensive: bool):
        self.model.is_intensive = is_intensive
        self.model.composition_model.is_intensive = is_intensive

    def predict_energy(self, atoms, energy=None, **kwargs):
        if energy is not None:
            return energy

        predictions = self.predict_all(atoms)
        return predictions["energy"]

    def predict_forces(self, atoms, forces=None, **kwargs):
        if forces is not None:
            return forces

        predictions = self.predict_all(atoms)
        return predictions["forces"]

    def predict_uncertainty(self, atoms, **kwargs):
        return 0

    def predict_uncertainty_forces(self, atoms, **kwargs):
        return np.zeros((len(atoms), 3))

    def train(self, data, *args, **kwargs):
        # Move to training device:

        # Training done with intensive energies (per atom).
        self.set_intensive(True)
        self.model.to(self.train_device)

        # Train the model:
        tuna = Tuna(self.model)

        if self.train_composition_model:
            tuna.train_composition_model(data)

        trainer_parameters = {
            "epochs": self.epochs,
            "learning_rate": self.learning_rate,
        }
        data_parameters = {
            "batch_size": self.batch_size,
            "val_ratio": 0.0,
            "train_ratio": 1.0,
        }

        if self.train_network:
            history = tuna.train_model(
                training_set=data,
                trainer_parameters=trainer_parameters,
                data_parameters=data_parameters,
            )

        # Move back to prediction device:
        self.model.to(self.predict_device)
        self.model.set_intensive(self.is_intensive)

    def converter(self, atoms, **kwargs):
        return self.predict_all(atoms)

    def predict_all(self, atoms, task='ef', atomic_features=False, crystal_features=True, **kwargs):
        structure = AseAtomsAdaptor.get_structure(atoms)
        graph = self.model.graph_converter(structure)
        model_prediction = self.model.predict_graph(
                        graph.to(self.predict_device), 
                        task=task, 
                        return_crystal_feas=crystal_features, 
                        return_atom_feas=atomic_features)

        if self.is_intensive:
            factor = len(atoms)
        else:
            factor = 1

        model_prediction['e'] = model_prediction['e']*factor

        results = {}
        if 'e' in task:
            results['energy'] = model_prediction['e']
        if 'f' in task:
            results['forces'] = model_prediction['f']        
        if atomic_features:
            results['atomic_features'] = model_prediction['atom_fea']
        if crystal_features:
            results['crystal_features'] = model_prediction['crystal_fea']

        atoms.info.update(model_prediction)
            
        return results
    
    def calculate(
        self,
        atoms: Atoms | None = None,
        properties: list | None = None,
        system_changes: list | None = None,
    ) -> None:
        """Calculate various properties of the atoms using CHGNet.

        Args:
            atoms (Atoms | None): The atoms object to calculate properties for.
            properties (list | None): The properties to calculate.
                Default is all properties.
            system_changes (list | None): The changes made to the system.
                Default is all changes.
        """
        properties = properties or all_properties
        system_changes = system_changes or all_changes
        Calculator.calculate(self,
            atoms=atoms,
            properties=properties,
            system_changes=system_changes,
        )

        model_prediction = self.predict_all(atoms)

        # Convert Result
        self.results.update(model_prediction)

    def get_atomic_features(self, atoms): 

        structures = [AseAtomsAdaptor.get_structure(atoms) for atoms in atoms]
        graphs = [self.model.graph_converter(struc) for struc in structures]

        model_prediction = self.model.predict_graph(
            graphs, task="e", return_atom_feas=True)
        
        return [pred['atom_fea'] for pred in model_prediction]

    def get_global_features(self, atoms):
        structures = [AseAtomsAdaptor.get_structure(atoms) for atoms in atoms]
        graphs = [self.model.graph_converter(struc) for struc in structures]

        model_prediction = self.model.predict_graph(
            graphs, task="e", return_crystal_feas=True)
        
        features = np.array([p['crystal_fea'] for p in model_prediction])
        
        return features

    def get_local_energies(self, atoms):

        structure = AseAtomsAdaptor.get_structure(atoms)
        graphs = self.model.graph_converter(structure)

        model_prediction = self.model.predict_graph(
            graphs, task="e", return_atom_feas=True, return_site_energies=True)
        
        return model_prediction['site_energies']
    
    def get_composition_energy(self, atoms):
        """
        Experimental! 
        """
        
        structure = AseAtomsAdaptor.get_structure(atoms)
        graph = self.model.graph_converter(structure)

        comp_energy = self.model.composition_model([graph])

        comp_site_energies = self.model.composition_model.get_site_energies([graph])
        
        return comp_energy.detach().numpy(), np.array(comp_site_energies)

    def batch_predict_all(self, data: List[Atoms], task='ef', batch_size=16, progress_bar=False):
        """
        Predicts the properties of a list of atoms objects.

        Arguments:
        batch: List[Atoms]
            The list of atoms objects to predict properties for.
        task: str
            The tasks to predict. Default is 'ef' (energy and forces). 
        """

        n_structures = len(data)

        # Check if atomic and crystal features are requested:
        atomic_features = 'a' in task
        crystal_features = 'c' in task
        site_energies = 'z' in task

        properties = list(task)
        if 'c' in task:
            properties[properties.index('c')] = 'crystal_fea'
        if 'a' in task:
            properties[properties.index('a')] = 'atom_fea'
        if 'z' in task:
            properties[properties.index('z')] = 'site_energies'

        # Setup results dictionary:
        naming_translation = {'e': 'energy', 
                              'f': 'forces', 
                              'atom_fea': 'atomic_features', 
                              'crystal_fea': 'crystal_features', 
                              'm': 'magmoms', 
                              's': 'stress', 
                              'site_energies': 'local_energies'}

        results = {prop:[] for key, prop in naming_translation.items() if key in properties}
        task = task.strip('a').strip('c').strip('z')

        # Loop over batches: 
        n_batches = int(np.ceil(len(data)/batch_size))

        if progress_bar:
            iterator = tqdm(range(n_batches))
        else:
            iterator = range(n_batches)

        for b in iterator:
            batch_indices = np.arange(b*batch_size, min([(b+1)*batch_size, n_structures]))
            batch = [data[i] for i in batch_indices]

            structures = [AseAtomsAdaptor.get_structure(atoms) for atoms in batch]
            graphs = [self.model.graph_converter(struc) for struc in structures]

            model_prediction = self.model.predict_graph(
                graphs, 
                task=task, 
                return_crystal_feas=crystal_features, 
                return_atom_feas=atomic_features, 
                return_site_energies=site_energies)

            if len(batch) == 1:
                model_prediction = [model_prediction]
            
            for mp in model_prediction:
                for key, prop in naming_translation.items():
                    if key in properties:
                        results[prop].append(mp[key])

        return results