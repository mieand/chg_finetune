try: 
    from gpaw.convergence_criteria import Energy, Forces
    from ase.dft.kpoints import monkhorst_pack
    from ase.calculators.calculator import kptdensity2monkhorstpack

    from gpaw import GPAW, PW
    GPAW_INSTALLED = True
except ImportError:
    GPAW_INSTALLED = False

from ase.optimize import BFGS
from ase.calculators.singlepoint import SinglePointCalculator

def get_gpaw_calculator(atoms):

    if not atoms.pbc.any():
        # If the structure is not periodic, use the gamma point.
        kpts = (1, 1, 1)
    else:
        # For periodic structure, use the mptrj setting.
        # Even though this is quite alot (I think?)
        kpts = kptdensity2monkhorstpack(atoms, kptdensity=3.5)



    cutoff = 520 # MPtrj setting. 

    convergence = {
        'energy': Energy(1e-6, relative=False), # MPtrj setting
        'density': 1.0e-5, # Better than GPAW default.
        'eigenstates': 4.0e-8, # GPAW default
        'bands': 'occupied'} # GPAW default

    calc = GPAW(
        mode=PW(cutoff), 
        kpts=kpts, 
        xc='PBE',
        convergence=convergence
        )
    
    return calc
                
def calculate(atoms):
    """
    Calculate energy and forces of atoms using GPAW with the MPtrj settings.
    """

    calc = get_gpaw_calculator(atoms)
    atoms.set_calculator(calc)

    original_pbc = atoms.pbc.copy()

    atoms.pbc = True
    forces = atoms.get_forces()
    energy = atoms.get_potential_energy()

    atoms.pbc = original_pbc
    atoms.calc = SinglePointCalculator(atoms, energy=energy, forces=forces)    

    return atoms

def optimize(atoms, trajectory=None, fmax=0.05, steps=1000):
    """
    Optimize the structure using the MPtrj settings.
    """
    calc = get_gpaw_calculator(atoms)

    atoms.pbc = True
    atoms.set_calculator(calc)

    optimizer = BFGS(atoms, logfile=None, trajectory=trajectory)
    optimizer.run(fmax=fmax, steps=steps)

    return atoms



