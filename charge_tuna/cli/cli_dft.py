from pathlib import Path
from ase.io import read, write
from charge_tuna.dft import calculate, optimize
from ase.parallel import parprint

def add_dft_parser(sub_parser):
    # Input arguments: 

    parser = sub_parser.add_parser('dft', help='Calculate DFT energies and forces')
    parser.set_defaults(func=dft_cli_main)
    
    parser.add_argument('input', help='Input trajectory file', type=str)
    parser.add_argument('index', help='Index of the atoms to calculate', type=int)
    parser.add_argument('directory', help='Output path', type=str)

    parser.add_argument('-o', '--optimize', help='Optimize the structure', action='store_true')
    parser.add_argument('-s', '--steps', help='Number of steps for optimization', type=int, default=1000)
    parser.add_argument('-f', '--fmax', help='Number of parallel processes', type=float, default=0.05)

def dft_cli_main(args):
    # Read atoms from input file
    parprint('\nRunning DFT with these settings: ')
    for key, val in vars(args).items():
        parprint(f'    {key}: {val}')

    atoms = read(args.input, index=args.index)

    # Name output file
    name = args.input.split('/')[-1].split('.')[0]
    out_path = Path(args.directory) / f'{name}-{args.index}.traj'
    out_path.parent.mkdir(parents=True, exist_ok=True)


    # Calculate energy and forces
    if args.optimize:
        parprint('Optimizing structure')
        trajectory_name = out_path.parent / f'{name}-{args.index}-opt.traj'
        atoms = optimize(atoms, trajectory=str(trajectory_name), fmax=args.fmax, steps=args.steps)
    else:
        atoms = calculate(atoms)

    # Write output file
    write(out_path, atoms)
