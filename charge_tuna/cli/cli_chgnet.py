from ase.io import read, write
from charge_tuna.agox_model import CHGNetModel
from charge_tuna.helpers import check_device

def add_chgnet_parser(sub_parser):
    # Input arguments: 

    parser = sub_parser.add_parser('evaluate', help='Calculate properties with CHGNet, such as total energy, local energies, local and global feaetures. All are added as info to the atoms objects.')
    parser.set_defaults(func=evaluate_cli_main)

    # Required arguments:    
    parser.add_argument('input', help='Input trajectory file', type=str)

    # Optional arguments:
    parser.add_argument('--output', '-o', help='Output trajectory file', type=str, default=None)
    parser.add_argument('--model_path', help='Path of model to use', type=str, default=None)
    parser.add_argument('--task', help='Properties to calculate', type=str, default='efacz')
    parser.add_argument('-tqdm', '--progress', help='Show progress bar', action='store_true')
    parser.add_argument('-b', '--batch_size', help='Batch size', type=int, default=16)

def evaluate_cli_main(args):

    # Read atoms from the input file:
    data = read(args.input, ':')

    print(f'Loaded data: {len(data)}')

    # Model: 
    model = CHGNetModel(model=args.model_path, predict_device=check_device())
    model.model.graph_converter.on_isolated_atoms = 'ignore'

    results = model.batch_predict_all(data, 
                                      task=args.task, 
                                      progress_bar=args.progress, 
                                      batch_size=args.batch_size)

    # Add the calculated properties as info on the atoms objects:
    for i, atoms in enumerate(data):
        for key, values in results.items():
            atoms.info[key] = values[i]

    # Write the results to the output directory:
    if args.output is None:
        name = args.input.split('/')[-1].split('.')[0]
        name += '-chgnet.traj'
    else:
        name = args.output

    write(f'{name}', data, format='traj')









    
