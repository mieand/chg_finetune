import torch

from charge_tuna.helpers import load_datasets, load_model, check_constraints
from chgnet.model import CHGNet
from charge_tuna.tune import Tuna
from charge_tuna.parameter_freezing import freeze_parameters
from charge_tuna.helpers import evaluate_model
from pathlib import Path


import numpy as np

def add_tune_parser(sub_parser):

    parser = sub_parser.add_parser('tune', help='Tune a model')
    parser.set_defaults(func=tune_cli_main)

    # Stuff about datasets:
    parser.add_argument('--training_set', nargs='+', required=True, type=str, help='Dataset to train the model on. One or multiple space seperated paths to trajectory files')
    parser.add_argument('--composition_set', nargs='+', required=False, type=str, default=None, help='Dataset to train the composition model on. One or multiple space seperated paths to trajectory files')
    parser.add_argument('--validation_set', nargs='+', required=False, type=str, default=None, help='Dataset to validate the model on. One or multiple space seperated paths to trajectory files')
    parser.add_argument('--test_set', nargs='+', required=False, type=str, default=None, help='Dataset to test the model on. One or multiple space seperated paths to trajectory files')
    parser.add_argument("--training_set_size", type=int, default=None)

    # Stuff about which model to start from:
    parser.add_argument('--model_path', type=str, default='0', help='Path to a pretrained model from which the training will start. If not given the default pretrained model is used..')

    # Stuff about naming & saving:
    parser.add_argument('--model_name', type=str, default='trained_model', help='Name of the model, corresponds to the directory where stuff will be saved.')
    parser.add_argument('--save_training_history', action='store_true', help='If set, the training history will be saved.')

    # Training hyperparameters:
    parser.add_argument('--freeze_level', type=int, default=1, help='Level of parameters to freeze.')
    parser.add_argument("--epochs", type=int, default=100, help='Number of epochs to train for.')
    parser.add_argument("--learning_rate", '-lr', type=float, default=1e-3, help='Iniital learning rate for training.')
    parser.add_argument("--batch_size", '-bs', type=int, default=8, help='Batch size of training batches.')
    parser.add_argument("--energy_loss_ratio", type=float, default=1.0, help='Ratio of energies in the loss function.')
    parser.add_argument("--force_loss_ratio", type=float, default=1.0, help='Ratio of forces in the loss function.')
    parser.add_argument('--fit_composition_model', type=bool, default=True, help='If set, the composition model will not be trained', choices=[True, False])
    parser.add_argument('--criterion', type=str, default='MSE', help='Loss function one of MSE, MAE, Huber')
    parser.add_argument('--learning_rate_scheduler', '-lrs', type=str, default='ReduceLROnPlateau', help='Learning rate scheduler to use.', choices=['Constant', 'ReduceLROnPlateau'])
    parser.add_argument('--optimizer', type=str, default='Adam', help='Optimizer to use, can be Adam, SGD or AdamW, RAdam', choices=['Adam', 'SGD', 'AdamW', 'RAdam'])
    parser.add_argument('--train_composition_model', action='store_true', help='If set, the composition model will be trained')


    parser.add_argument('--experimental_comp', action='store_true')

    # Seeding:
    parser.add_argument('--seed', type=int, default=None)

    # Unsafe mode: 
    parser.add_argument('--unsafe', action='store_true', help='UNSAFE MODE')

def _get_trainer_parameters(args):
    # Train the rest of the model:
    
    assert args.criterion in ['MSE', 'MAE', 'Huber'], f'Criterion {args.criterion} not recognized. Must be one of MSE, MAE, Huber'

    trainer_parameters = {
        'epochs': args.epochs, 
        'learning_rate': args.learning_rate,
        'energy_loss_ratio': args.energy_loss_ratio,
        'force_loss_ratio': args.force_loss_ratio,
        'criterion': args.criterion,
        'scheduler': args.learning_rate_scheduler,
        }
    return trainer_parameters

def _get_data_parameters(args):
    data_parameters = {
        'batch_size': args.batch_size,
        'val_ratio': 0.1,
        'train_ratio': 0.9,
    }
    return data_parameters

def _check_testset(tuna, test_set, name, suffix=None):
    if suffix is None:
        suffix = ''

    test_results = tuna.evaluate_model(test_set)
    for key, val in test_results.items():
        if key in ['RMSE', 'MAE', 'Force MAE', 'Force RMSE']:
            per_atom = test_results.get(key+"_per_atom", None)
            if per_atom is not None:
                print(f'    {key}: {val:.5f} ({per_atom:.5f})')
            else:
                print(f'    {key}: {val:.5f}')
    print('', flush=True)
    np.savez(name+f'/test_results_{suffix}.npz', **test_results)

def _setup_datasets(args):
     # Datasets:
        
    training_set = load_datasets(args.training_set)

    # Pick 90% of the training set at random:
    training_indices = np.random.choice(len(training_set), int(0.8*len(training_set)), replace=False)
    validation_indices = np.array([i for i in range(len(training_set)) if i not in training_indices])

    validation_set = [training_set[i] for i in validation_indices]
    training_set = [training_set[i] for i in training_indices]
    
    if args.training_set_size is not None:
        np.random_shuffle(training_set)
        training_set = training_set[:args.training_set_size]

    if args.composition_set is not None:
        composition_set = load_datasets(args.composition_set)    
    else:
        composition_set = training_set

    # Testset: 
    if args.test_set is not None:
        test_set = load_datasets(args.test_set)
    else:
        test_set = None

    # Validation set:
    # if args.validation_set is not None:
    #     validation_set = load_datasets(args.validation_set)
    # else:
    #     validation_set = None


    datasets =  {'training': training_set, 'composition': composition_set, 'validation': validation_set, 'test': test_set}
    print('\nDatasets:')
    for name, dataset in datasets.items():
        if dataset is not None:
            energies = np.array([atoms.get_potential_energy() for atoms in dataset])
            print(f'    {name}-set: {len(dataset)} {energies.min():2.3f} {energies.mean():2.3f} {energies.max():2.3f}')
    print('', flush=True)

    return datasets


def _check_constraints(datasets):
    print('\nChecking constraints:')
    for name, dataset in datasets.items():
        if dataset is not None:
            check = check_constraints(dataset)    
            if check == False:
                raise ValueError(f'Forces likely improperly stored in {name} dataset. Check how the data was stored. Stored forces on constrained atoms are zero.')
            else:
                print(f'    {name}-set: Forces properly stored')
    print('', flush=True)

def tune_cli_main(args):

    # Report on input settings:
    print('\nRunning Tuna with these settings: ')
    for key, val in vars(args).items():
        print(f'    {key}: {val} ({type(val)})')

    # Set seeds: 
    if args.seed is not None:
        np.random.seed(args.seed)
        torch.random.seed(args.seed)

    # Datasets:
    datasets = _setup_datasets(args)

    if not args.unsafe:
        _check_constraints(datasets)

    # Setup:
    if args.model_path == '-1':
        model = CHGNet()
    elif args.model_path != '0':
        model = load_model(args.model_path)
    else:
        model = CHGNet.load()

    # Make the directory to store stuff:
    model_directory = Path(args.model_name)
    model_directory.mkdir(parents=True, exist_ok=True)

    # Freeze some parameters:
    freeze_parameters(model, freeze_level=args.freeze_level)

    # Tuna:
    tuna = Tuna(model=model)
    
    # Train the composition model:
    if args.fit_composition_model:
        tuna.train_composition_model(datasets['composition'], args.experimental_comp)

    # Evaluate the composition model:
    if datasets['test'] is not None:
        print('\nTest results after training composition model:')
        _check_testset(tuna, datasets['test'], args.model_name, suffix='comp')

    history = tuna.train_model(training_set=datasets['training'], 
                               validation_set=datasets['validation'],
                               test_set=datasets['test'],
                               trainer_parameters=_get_trainer_parameters(args),
                               data_parameters=_get_data_parameters(args),
                               save_dir=args.model_name)

    if args.save_training_history:
        np.savez(args.model_name+'/training_history.npz', **history)

    # Evaluate the model on the test-set:
    if datasets['test'] is not None:
        print('\nTest results after training:')
        _check_testset(tuna, datasets['test'], args.model_name, suffix='after_training')



