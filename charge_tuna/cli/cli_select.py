import numpy as np
from timeit import default_timer as dt
from charge_tuna.selection import uniform_energy_selection, kmeans_selection, fps_selection
from ase.calculators.singlepoint import SinglePointCalculator

def add_select_parser(subparsers):
    parser = subparsers.add_parser('select', help='Select a subset of configurations')

    subsubparser = parser.add_subparsers(dest='selection_method', help='Selection method')

    def add_common_arguments(subparser):
        subparser.add_argument('-p', '--path', type=str, help='Input trajectory file path.')
        subparser.add_argument('-o', '--output', default=None, type=str, help='Output trajectory file path')
        subparser.add_argument('-et', '--energy_threshold', type=float, help='Energy threshold for selection', default=5)
        subparser.add_argument('-v', '--verbose', action='store_true', help='Print verbose output')

    # Uniform energy selection
    parser = subsubparser.add_parser('uniform', help='Select a subset of configurations with uniform energy distribution')
    add_common_arguments(parser)
    parser.add_argument('-es', '--energy_step', type=float, help='Energy window for selection', default=0.1)
    parser.set_defaults(func=cli_select_main)

    # Kmeans clustering selection
    parser = subsubparser.add_parser('kmeans', help='Select a subset of configurations with kmeans clustering')
    add_common_arguments(parser)
    parser.add_argument('-n', '--n_clusters', type=int, help='Number of clusters', default=50)
    parser.set_defaults(func=cli_select_main)

    # Farthest point smapling selection
    parser = subsubparser.add_parser('fps', help='Select a subset of configurations with farthest point sampling')
    add_common_arguments(parser)
    parser.add_argument('-n', '--n_samples', type=int, help='Number of samples', default=50)
    parser.set_defaults(func=cli_select_main)

def cli_select_main(args):

    from ase.io import read, write

    # Read the structures
    t0 = dt()
    data = read(args.path, ':')
    t1 = dt()
    if args.verbose:
        print(f'Read {len(data)} structures in {t1 - t0:4.1f} seconds')

    # Filter the structures based on energy
    energies = np.array([atoms.get_potential_energy() for atoms in data])
    allowed_indices = np.argwhere(energies < energies.min() + args.energy_threshold).flatten()
    allowed_data = [data[i] for i in allowed_indices]

    t2 = dt()    
    if args.selection_method == 'kmeans':
        selected = kmeans_selection(allowed_data, args.n_clusters)
    elif args.selection_method == 'uniform':
        selected = uniform_energy_selection(allowed_data, args.energy_step)
    elif args.selection_method == 'fps':
        selected = fps_selection(allowed_data, args.n_samples)
    else:
        raise ValueError(f'Unknown selection method {args.selected_method}')
    t3 = dt()
    if args.verbose:
        print(f'Selected {len(selected)} structures in {t3 - t2:4.1f} seconds')

    for atoms in selected:
        energy = atoms.get_potential_energy()
        forces = atoms.get_forces(apply_constraint=False)

        if not atoms.pbc.any():
            atoms.center()

        calc = SinglePointCalculator(atoms, energy=energy, forces=forces)
        atoms.set_calculator(calc)

    # Sort according to energies:
    selected.sort(key=lambda atoms: atoms.get_potential_energy())

    if args.output is None:
        out = args.path.split('.')[0] + '-selected.traj'
    else:
        out = args.output

    write(out, selected)