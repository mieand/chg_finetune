from ase.io import read, write
from pathlib import Path
import subprocess
import re

from charge_tuna.config import cfg

def get_base_script():
    submit_script="""#!/bin/sh
#SBATCH --job-name=NAME
#SBATCH --partition=PARTITION
#SBATCH --mem=MEMORY
#SBATCH --nodes=1
#SBATCH --time=TIME
#SBATCH --ntasks=NTASKS
#SBATCH --array=0-ARR_END%ARR_TOTAL
#SBATCH --output=out/slurm-%A_%a.out
#SBATCH --no-requeue 
#SBATCH --gres=gpu:0
#SBATCH --export=None

echo "========= Job started  at `date` =========="
echo "Job array ID: $SLURM_ARRAY_TASK_ID"

SOURCE
mpiexec tuna dft INPUT $SLURM_ARRAY_TASK_ID . OPTIMIZE -s STEPS -f FMAX

echo "========= Job finished at `date` =========="
"""
    return submit_script

def get_combine_job_script():
    submit_script="""#!/bin/sh
#SBATCH --job-name=combine_NAME
#SBATCH --partition=qtest
#SBATCH --mem=10G
#SBATCH --nodes=1
#SBATCH --time=00:10:00
#SBATCH --ntasks=1
#SBATCH --output=out/slurm-%A.out
#SBATCH --no-requeue 
#SBATCH --export=None
#SBATCH --dependency=afterany:JOB_ID

echo "========= Job started  at `date` =========="

SOURCE
tuna combine -n NAME-{}.traj -t ARR_TOTAL --output OUTPUT-combined.traj
echo "========= Job finished at `date` =========="
"""
    return submit_script

def add_submit_parser(sub_parser):
    parser = sub_parser.add_parser('submit', help='Submit DFT calculations to the cluster')
    parser.set_defaults(func=cli_submit_main)
    
    parser.add_argument('input', help='Input trajectory file', type=str)
    parser.add_argument('directory', help='Output directory', type=str)
    parser.add_argument('--memory', help='Memory per node', type=str, default='20G')
    parser.add_argument('--time', help='Time per job', type=str, default='01:00:00')
    parser.add_argument('-n', '--ntasks', help='Number of tasks per node', type=int, default=12)
    parser.add_argument('--arr_total', help='Total number of jobs', type=int, default=None)
    parser.add_argument('--write_combine_only', action='store_true', help='Write combine script only')
    parser.add_argument('-p', '--partition', help='Partition', type=str, default='q48,q40,q36,q28')
    parser.add_argument('-o', '--optimize', help='Optimize the structure', action='store_true')

    parser.add_argument('-s', '--steps', help='Number of steps for optimization', type=int, default=1000)
    parser.add_argument('-f', '--fmax', help='Number of parallel processes', type=float, default=0.05)


def cli_submit_main(args):

    name = args.input.split('/')[-1].split('.')[0]

    input_path = Path(args.input)
    if not input_path.exists():
        raise FileNotFoundError(f'Input file {args.input} does not exist')

    input_path = input_path.resolve()
    args.input = str(input_path)

    script = get_base_script()
    script = script.replace('NAME', name)

    args.source = cfg.parser['source']['source']

    args.arr_end = len(read(args.input, index=':')) - 1 
    if args.arr_total is None:
        args.arr_total = args.arr_end + 1

    if args.optimize:
        args.optimize = '-o'
    else:
        args.optimize = ''

    for arg in vars(args):
        key = arg.upper()
        value = getattr(args, arg)
        script = script.replace(key, str(value))

    directory = Path(args.directory)
    directory.mkdir(parents=True, exist_ok=True)

    if not args.write_combine_only:
        with open(directory / 'submit.sh', 'w') as f:
            f.write(script)

        print(f'Submit script written to {directory / "submit.sh"}')
        output = str(subprocess.check_output(['sbatch', 'submit.sh'], cwd=directory))
        job_ID = int(re.search(r'\d+', output).group())
        print('Job ID: {}'.format(job_ID))

    # Combine script: 
    combine_job_script = get_combine_job_script()
    combine_job_script = combine_job_script.replace('NAME', name)
    combine_job_script = combine_job_script.replace('JOB_ID', str(job_ID))
    combine_job_script = combine_job_script.replace('OUTPUT', name)
    combine_job_script = combine_job_script.replace('ARR_TOTAL', str(args.arr_total))
    combine_job_script = combine_job_script.replace('SOURCE', args.source)
    with open(directory / 'combine.sh', 'w') as f:
        f.write(combine_job_script)

    output = str(subprocess.check_output(['sbatch', 'combine.sh'], cwd=directory))
