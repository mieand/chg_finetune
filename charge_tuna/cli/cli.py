from argparse import ArgumentParser
from charge_tuna.cli.cli_tune import add_tune_parser
from charge_tuna.cli.cli_dft import add_dft_parser
from charge_tuna.cli.cli_dft_submit import add_submit_parser
from charge_tuna.cli.cli_select import add_select_parser
from charge_tuna.cli.cli_combine import add_combine_parser
from charge_tuna.cli.cli_chgnet import add_chgnet_parser

def main():

    parser = ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    add_tune_parser(subparsers)
    add_dft_parser(subparsers)
    add_submit_parser(subparsers)
    add_select_parser(subparsers)
    add_combine_parser(subparsers)
    add_chgnet_parser(subparsers)

    args = parser.parse_args()
    args.func(args)




if __name__ == '__main__':
    main()

