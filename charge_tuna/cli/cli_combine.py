from argparse import ArgumentParser
from ase.io import read, write
from ase.io.formats import UnknownFileTypeError

def add_combine_parser(sub_parser):
    parser = sub_parser.add_parser('combine', help='Combine trajectory files')
    parser.set_defaults(func=combine_cli_main)
    
    # Multiple arguments possible:
    parser.add_argument('-n', '--name', nargs='+', help='Name of the trajectory files', type=str)
    parser.add_argument('-t', '--total', help='Total number of trajectory files', type=int, default=None)

    parser.add_argument('--output', help='Output file name', type=str, default='combined.traj')
    parser.add_argument('-m', '--mode', default='last', type=str)


def combine_cli_main(args):

    if args.total is None:
        files = args.name
    else:
        base_name = args.name[0].split('.')[0] + '.traj'
        files = [base_name.format(i) for i in range(args.total)]

    if args.mode == 'last':
        combined = []
        for i, name in enumerate(files):
            try:
                atoms = read(name.format(i))
                atoms.info['org_index'] = i
                combined.append(atoms)
            except FileNotFoundError:
                pass
    elif args.mode == 'all':
        combined = []
        for i, name in enumerate(files):
            try:
                atoms = read(name.format(i), ':')
                combined.extend(atoms)
            except FileNotFoundError:
                pass
            except UnknownFileTypeError:
                continue

    print(f'Writing combined trajectory file: {len(combined)} atoms')

    write(args.output, combined)
