import numpy as np
import torch
from chgnet.model.composition_model import AtomRef
from charge_tuna.helpers import check_device, convert_to_structure, evaluate_model

from scipy.optimize import minimize

mptraj = [
    -3.4431,
    -0.1279,
    -2.8300,
    -3.4737,
    -7.4946,
    -8.2354,
    -8.1611,
    -8.3861,
    -5.7498,
    -0.0236,
    -1.7406,
    -1.6788,
    -4.2833,
    -6.2002,
    -6.1315,
    -5.8405,
    -3.8795,
    -0.0703,
    -1.5668,
    -3.4451,
    -7.0549,
    -9.1465,
    -9.2594,
    -9.3514,
    -8.9843,
    -8.0228,
    -6.4955,
    -5.6057,
    -3.4002,
    -0.9217,
    -3.2499,
    -4.9164,
    -4.7810,
    -5.0191,
    -3.3316,
    0.5130,
    -1.4043,
    -3.2175,
    -7.4994,
    -9.3816,
    -10.4386,
    -9.9539,
    -7.9555,
    -8.5440,
    -7.3245,
    -5.2771,
    -1.9014,
    -0.4034,
    -2.6002,
    -4.0054,
    -4.1156,
    -3.9928,
    -2.7003,
    2.2170,
    -1.9671,
    -3.7180,
    -6.8133,
    -7.3502,
    -6.0712,
    -6.1699,
    -5.1471,
    -6.1925,
    -11.5829,
    -15.8841,
    -5.9994,
    -6.0798,
    -5.9513,
    -6.0400,
    -5.9773,
    -2.5091,
    -6.0767,
    -10.6666,
    -11.8761,
    -11.8491,
    -10.7397,
    -9.6100,
    -8.4755,
    -6.2070,
    -3.0337,
    0.4726,
    -1.6425,
    -3.1295,
    -3.3328,
    -0.1221,
    -0.3448,
    -0.4364,
    -0.1661,
    -0.3680,
    -4.1869,
    -8.4233,
    -10.0467,
    -12.0953,
    -12.5228,
    -14.2530,
]


def fit_composition_model(model, data, structures=None, verbose=False):

    if structures is None:
        structures = convert_to_structure(data)
    energies = np.array([atoms.get_potential_energy() for atoms in data])
    n_atoms = np.array([len(atoms) for atoms in data])

    with torch.no_grad():
        model.composition_model.fc.weight[0, :] = 0

    predictions = evaluate_model(data, model)
    targets = (energies - predictions) / n_atoms

    new_atom_ref = AtomRef(is_intensive=True)
    new_atom_ref.initialize_from_MPtrj()

    new_atom_ref.fit(structures, targets)

    model.composition_model = new_atom_ref
    model.to(check_device())

    if verbose:
        print("")
        print("Composition Model Weights:")
        w = model.composition_model.fc.weight.detach().cpu().numpy().squeeze()
        indices = np.argwhere(w != 0).flatten()
        for index in indices:
            print(f"    Element {index+1}: {w[index]}")

    return new_atom_ref


def optimize_composition_model(model, data, structures=None, verbose=True):
    print('Optimizing comp model', flush=True)

    # Convert to structures:
    if structures is None:
        structures = convert_to_structure(data)

    # Get the real energies:
    true_energies = np.array([atoms.get_potential_energy() for atoms in data])

    # Get the different species in the data:
    all_numbers = np.concatenate([atoms.get_atomic_numbers() for atoms in data])
    unique_numbers = np.unique(all_numbers)

    number_count = np.zeros((len(data), len(unique_numbers)))
    for i, atoms in enumerate(data):
        atomic_numbers = atoms.get_atomic_numbers()
        for j, unique_number in enumerate(unique_numbers):
            number_count[i, j] = np.count_nonzero(atomic_numbers == unique_number)

    # Remove the composition model from the given model:
    model.composition_model = None

    # Get the predicted energies:
    print('Starting to calculate interaction energy', flush=True)
    interaction_energy = evaluate_model(data, model)

    print('Calculated interaction energy', flush=True)

    # Loss function to optimize the composition model:
    def loss(weights):

        comp_energy = number_count @ weights
        total_energy = interaction_energy + comp_energy

        mse = np.mean((true_energies - total_energy) ** 2)
        return mse

    # Initial guess from MPtrj
    initial_guess = np.zeros(len(unique_numbers))
    for i, unique_number in enumerate(unique_numbers):
        initial_guess[i] = mptraj[unique_number-1]

    # Do the optimization:
    opt = minimize(loss, initial_guess)

    # Make a new AtomRef model:
    model.composition_model = AtomRef(is_intensive=True)

    with torch.no_grad():
        model.composition_model.fc.weight[0, :] = 0
        for i, number in enumerate(unique_numbers):
            model.composition_model.fc.weight[0, number - 1] = opt.x[i]

    model.composition_model.fitted = True

    if verbose:
        print("")
        print("Composition Model Weights:")
        w = model.composition_model.fc.weight.detach().cpu().numpy().squeeze()
        indices = np.argwhere(w != 0).flatten()
        for index in indices:
            print(f"    Element {index+1}: {w[index]}")

        print(f"Loss: {loss(opt.x)}")

    model.to(check_device())

    return model.composition_model


if __name__ == "__main__":

    from chgnet.model import CHGNet
    from ase.io import read

    model = CHGNet.load()
    data = read(
        "/mnt/lustre/grnfs0/users/machri/projects/universal_models/datasets/combined_Cu6O4.traj",
        ":",
    )
    fit_composition_model(model, data)
