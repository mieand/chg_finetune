## Meeting 12/02/2024:

My general strategy has been to: 

* Run parallel tempering searches for various sizes of Au-clusters (10, 15, 20, 25, 30, 40 atoms) in CHGNet. 
* Select a small energitically diverse sample to do DFT for. (0.1 eV intervals up to 5 eV over the best CHGNet energy for each size).
* Fine-tune on that to get the next generation model and repeat the process. 

### Fine-tuning settings: 

My fine-tuning settings are: 

* Using Huber loss rather than MSE (Because CHGNet is trained on MPtrj with Huber loss)
* Initial learning rate of 0.001
* Batch size of 16.
* Equal energy/force loss-ratios. 
* 100 epochs 
* 0.9/0.1 train/validation split. Next generation model is the one with the lowest energy validation error. 

### Generation 0: 

After selecting ~300 structures for DFT I fine-tune and get this loss curve: 

![loss_curve](figures/loss_gen0.png)

The parity plots after training look like so: 

![parity_generation_0](figures/parity_gen0.png)

### Generation 1: 

The parity plot with data from the 0th and 1st generation looks quite promising - especially for the larger 
clusters the best DFT structure improves by quite a bit. 

![parity_generation_1](figures/parity_gen1.png)

### Generation 2: 

![parity_generation_2](figures/parity_gen2.png)

### Generation 3: 

For the third generation I made a few changes: 

* Relaxations during the parallel tempering search get 300 steps rather than 100, so more sure of getting converged local minima of CHGNet. 
* I select structure within 2.5 eV with steps of 0.05 eV of the best for each cluster size.

Furthermore, for the smaller clusters I DFT relaxed all of the selected configurations. 

![parity_generation_2](figures/parity_gen3.png)

The dashed lines in the Au20 parity plot show the DFT and CHGNet energy of the GM structure. 

I have extracted the 'crystal-feature' of CHGNet for the ~25k structures produced. I can then make a PCA plot 
of these structures with the networks own representation of them 

![pca_au20](figures/pca_Au20.png)

I have also tried using this feature to see if any of the ~25k structures are in fact the GM 
structure, and I have not been able to find an exact match. The closest one (8th in the feature) is a pyramid with a missing 
corner.

The GM structure is a minimum in CHGNet but it is 1 eV'ish higher in energy than some other structures that it finds. 

### Overall: 

![gen_evo](figures/generation_evo.png)

I think the most difficult step is how to select which structures to do DFT for. 

I think using the networks internal representation (the 'crystal feature'/global feature) could be an avenue. 

However, there are two distinct things to select data for: 
* For further fine-tuning
* For determining the global minimum. 


### Parallel Tempering:

Did some analysis of the PT runs: 

![pt_dist](figures/pt_dist.png)

![pt_mean](figures/pt_mean.png)

### Data selection: 

![au20_pca](figures/Au20_pca.png)





