from ase import Atoms
from agox.candidates import StandardCandidate
from agox.postprocessors import ParallelRelaxPostprocess
from ase.optimize import BFGS
from ase.io import read
from charge_tuna import CHGNetModel
from timeit import default_timer as dt

path = '/home/machri/Projects/agox/data/chgnet_search/CuO_small/fine_tune/test/epoch99_e12_f61_sNA_mNA.pth.tar'

model = CHGNetModel(
    # model=None, # Use pre-trained model.
    model=path,
    epochs=100,
    learning_rate=1e-3,
    train_composition_model=False,
    batch_size=8,
    synchronize=True,
)

relaxer = ParallelRelaxPostprocess(
    model,
    cpu_count=4,
    optimizer=BFGS,
    optimizer_run_kwargs={"steps": 10},
    use_counter=False,
    fix_template=True,
)

# All of this happens automatically in an AGOX run, but we need to do it manually here:
relaxer.pool.update_module_interconnections()
relaxer.pool.update_modules(writer=print)
relaxer.pool.print_modules(writer=print)

# Load training set:
training_dataset = "/mnt/lustre/grnfs0/users/machri/projects/universal_models/datasets/combined_Cu6O4.traj"
data = read(training_dataset, index=":")
template = Atoms(cell=data[0].cell, pbc=True)
data = [StandardCandidate.from_atoms(template, atoms) for atoms in data]

E0 = model.predict_energy(data[0])

# Relax a few steps with the pre-fit model:
t0 = dt()
before_data = [atoms.copy() for atoms in data[0:4]]
before = relaxer.process_list(before_data)
t1 = dt()
print(f'Relaxation time: {t1-t0:.3f}', flush=True)

# Train the model:
t0 = dt()
model.train(data)
t1 = dt()
print(f'Training time: {t1-t0:.3f}')

E1 = model.predict_energy(data[0])

# Again this happens automatically in a run, but do it manually here: 
relaxer.pool.update_modules(writer=print)

t0 = dt()
after_data = [atoms.copy() for atoms in data[0:4]]
after = relaxer.process_list(after_data)
t1 = dt()
print(f'Relaxation time: {t1-t0:.3f}', flush=True)

before_energies = [atoms.get_potential_energy() for atoms in before]
after_energies = [atoms.get_potential_energy() for atoms in after]

print(f"{before_energies = }")
print(f"{after_energies = }")