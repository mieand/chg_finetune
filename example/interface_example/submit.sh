#!/bin/bash
#SBATCH --job-name=tuna
#SBATCH --partition=qa100,qgpu
#SBATCH --mem=10G
#SBATCH --tasks=4
#SBATCH --nodes=1
#SBATCH --time=02:00:00
#SBATCH --gres=gpu:1
#SBATCH --output=slurm-%j.out

# example submit script for tuna 

source ~/.bashrc
source ~/source_files/agox-chgnet

python script.py