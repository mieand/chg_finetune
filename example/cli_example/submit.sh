#!/bin/bash
#SBATCH --job-name=tuna
#SBATCH --partition=qa100,qgpu
#SBATCH --mem=10G
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --time=00:10:00
#SBATCH --gres=gpu:1
#SBATCH --output=slurm-%j.out

# example submit script for tuna 

# training_dataset='/mnt/lustre/grnfs0/users/machri/projects/universal_models/datasets/combined_Cu6O4.traj'

# training_dataset='/home/hammer/runs/2024/cuo_chgnet/train_on_cu4Xo1X_00/cu4Xo1X_00.traj /home/hammer/runs/2024/cuo_chgnet/train_on_cu4Xo1X_01/cu4Xo1X_01.traj /home/hammer/runs/2024/cuo_chgnet/train_on_cu4Xo1X_02/cu4Xo1X_02.traj'

training_dataset='/home/hammer/runs/2024/cuo_chgnet/train_on_cu4Xo1X_00/cu4Xo1X_00.traj /home/hammer/runs/2024/cuo_chgnet/train_on_cu4Xo1X_01/cu4Xo1X_01.traj'


source ~/.bashrc
source ~/source_files/agox-chgnet

tuna --training_set $training_dataset \
    --composition_set $training_dataset \
    --test_set $training_dataset \
    --model_name 'test_old_comp' \
    --epochs 1 \
    --learning_rate 0.001 \
    --batch_size 4 \
    --save_training_history \
    --criterion 'Huber' \
    # --experimental_comp