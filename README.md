# Charge Tuna

Fine-tuning of CHGNet and AGOX inteface.

1. [Installation](#installation)
2. [CLI Tools](#cli-tools)
3. [Parallel Tempering](#parallel-tempering-with-chgnet-model)

## Installation:

Recommended to install in development mode, after cloning and entering the directory
```
pip install -e 
```

Some of the CLI tools require a little setup as they can submit jobs to Grendel. 
To make them work you need to copy the `example_config.ini` file to `config.ini`
and update the `source`-key of the `[source]` section: 

```
[source]
source = ml purge
         export OMP_NUM_THREADS=1
         source /comm/swstack/bin/modules.sh --silent
         source /comm/groupstacks/gpaw/bin/modules.sh
         ml python/3.11.1
         ml gpaw-gcc-mkl/23.9.1
         source ~/envs/agox-chgnet/bin/activate # Change this path to your environment
```
This specifies which modules and environment with be loaded for submitted jobs. 


## CLI Tools: 

After installation the package with pip, a number of CLI tools become available: 

* `tuna tune`: For fine-tuning. 
* `tuna dft`: For doing a single GPAW calculation with approximate MPtrj settings. 
* `tuna select`: For select a subset of configurations from a trajectory file, based on energy.
* `tuna submit`: For submitting an array-job to Grendel of GPAW DFT calculations. 
* `tuna combine`: For merging trajectory files. 

## Fine-tuning CLI: 

After installation fine-tuning can be done with the CLI tool, like so: 

```bash
tuna tune --training_set $training_dataset \
    --composition_set $composition_dataset \
    --test_set $test_dataset \
    --validation_set $validation_dataset \
    --model_name 'gen_0' \
    --epochs 100 \
    --learning_rate 0.001 \
    --batch_size 8 \
    --save_training_history \
    --criterion 'Huber' \
```

Specifying all four sets is optional, only the training set is required in 
which case the model will have its composition model fit on the training set. 

To see all inputs use `tuna tune --help`. 

## Selection CLI: 

```bash 
tuna select trajectory.traj dE Emax
```
Will select structures `trajectory.traj` in an energy span defined by `dE` and `Emax` are energy intervals and the maximum energy, 
the selected structures will be saved `trajectory-selected.traj`.

## DFT Submission CLI: 

```bash
tuna submit trajectory.traj output_folder
```
Will submit DFT calculations for all the structures in `trajectory.traj` and write outputs in `output_folder`, when all the 
calculations are done they will be automatically combined into a single trajectory, `output_folder/trajectory-combined.traj`. 

As the DFT calculation may fail, the `Atoms`-objects in the combined trajectory folder will have 
`org_index` in `atoms.info` with the structures index in `trajectory.traj`. 

To do DFT relaxations, the optional flag `--optimize` can be added. 
```bash
tuna submit trajectory.traj output_folder --optimize
```

## Fine-tuning Python: 

As an alternative to the CLI, you can use the `Tuna`-class in a script:

```python
# Tuna:
tuna = Tuna(model=model)
    
# Train the composition model:
tuna.train_composition_model(composition_set)

# Train the model: 
history = tuna.train_model(training_set=training_set, 
                        validation_set=validation_set,
                        test_set=test_set,
                        trainer_parameters=trainer_parameters, 
                        data_parameters=data_parameters,
                        save_dir=args.model_name)
```

## Parallel Tempering with CHGNet Model: 

I have written a new parallel-tempering sampler for non-concurrent runs, that is 
the parallelization is done with Ray not with different slurm jobs. 

This is on the `ray_parallel_tempering`-branch of AGOX. 

The script below is an example of such a PT-search: 

```python
import matplotlib

matplotlib.use("Agg")

import numpy as np
from agox import AGOX
from agox.databases import Database
from agox.environments import Environment
from agox.generators import RandomGenerator, RattleGenerator
from agox.collectors import ParallelCollector, ParallelTemperingCollector
from agox.postprocessors import ParallelRelaxPostprocess
from agox.samplers.parallel_tempering import ParallelTemperingSampler
from agox.postprocessors.minimum_dist import MinimumDistPostProcess
from agox.postprocessors import CenteringPostProcess

from charge_tuna.agox_model import CHGNetModel

# Using argparse if e.g. using array-jobs on Slurm to do several independent searches.
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument('-i', '--run_idx', type=int, default=0)
args = parser.parse_args()

seed = args.run_idx
database_index = args.run_idx

################################################################################
# Global settings:
N_Au = 40
sample_size = 10

N_rattle_fraction = np.linspace(0.25, 0.85, sample_size)
rattle_amplitude = np.linspace(3, 5, sample_size)

model_path = "/home/machri/Projects/agox/data/chgnet_search/gold_clusters/generation_2/gen_2/bestE_epoch54_e16_f103_sNA_mNA.pth.tar"

confinement_size = 10
cell_size = 15

################################################################################
# Calculator
##############################################################################

# To use the pretrained model:
calc = CHGNetModel(model=model_path, synchronize=False) # Synchronize is False, because we not updating the model during the search.

##############################################################################
# System & general settings:
##############################################################################
from ase import Atoms

template = Atoms(cell=np.eye(3)*cell_size)
confinement_cell = np.eye(3) * confinement_size
confinement_corner = np.ones(3) * (cell_size - confinement_size) / 2

environment = Environment(
    template=template,
    symbols=f"Au{N_Au}",
    confinement_cell=confinement_cell,
    confinement_corner=confinement_corner,
)

# Database
db_path = "db{}.db".format(database_index)  # From input argument!
# The database stores 'candidates' rather than 'evaluated_candidates', so we don't need an Evaluator. 
database = Database(filename=db_path, order=4, gets={'get_key':'candidates'})

##############################################################################
# Search Settings:
##############################################################################

# Sampler to choose candidates to modify using the generators.
temperatures = np.linspace(0.01, 5, sample_size)
sampler = ParallelTemperingSampler(temperatures, 
                                   sample_size=sample_size, 
                                   order=3, verbose=True)

# Generators to produce candidates structures
random_generator = RandomGenerator(**environment.get_confinement())

# Dict specificies how many candidates are created with and the dict-keys are iterations.
generator_dict = {}
generator_dict['random'] = random_generator

for i, t in enumerate(temperatures):
    generator_dict[t] = RattleGenerator(**environment.get_confinement(), 
                                        rattle_amplitude=rattle_amplitude[i], 
                                        n_rattle=int(N_rattle_fraction[i]*N_Au))

collector = ParallelTemperingCollector(
    generator_dict,
    sampler=sampler,
    environment=environment,
    cpu_count=sample_size,
    order=1,
)


from ase.optimize import BFGS
relaxer = ParallelRelaxPostprocess(
    model=calc,
    constraints=environment.get_constraints(),
    optimizer_run_kwargs={"steps": 300, "fmax": 0.025}, # Choose this appropriately.
    start_relax=0,
    optimizer=BFGS,
    order=2,
)

centerer = CenteringPostProcess(
    order=1.5,
)

minimum_dist = MinimumDistPostProcess(
    order=2.5,
)

##############################################################################
# Let get the show running!
##############################################################################

# The oder of things here does not matter. But it can be simpler to understand
# what the expected behaviour is if they are put in order.
agox = AGOX(collector, relaxer, database, sampler, centerer, minimum_dist, seed=seed)

agox.run(N_iterations=250)
```

## Training on GPU and parallel relaxation on CPU. 
```python
from ase import Atoms
from agox.candidates import StandardCandidate
from agox.postprocessors import ParallelRelaxPostprocess
from ase.optimize import BFGS
from ase.io import read
from charge_tuna import CHGNetModel

path = '/home/machri/Projects/agox/data/chgnet_search/CuO_small/fine_tune/test/epoch99_e12_f61_sNA_mNA.pth.tar'

model = CHGNetModel(
    # model=None, # Use pre-trained model.
    model=path,
    epochs=100,
    learning_rate=1e-3,
    train_composition_model=False,
    batch_size=8,
    synchronize=True,
)

relaxer = ParallelRelaxPostprocess(
    model,
    cpu_count=4,
    optimizer=BFGS,
    optimizer_run_kwargs={"steps": 10},
    use_counter=False,
    fix_template=True,
)

# All of this happens automatically in an AGOX run, but we need to do it manually here:
relaxer.pool.update_module_interconnections()
relaxer.pool.update_modules(writer=print)
relaxer.pool.print_modules(writer=print)

# Load training set:
training_dataset = "/mnt/lustre/grnfs0/users/machri/projects/universal_models/datasets/combined_Cu6O4.traj"
data = read(training_dataset, index=":")
template = Atoms(cell=data[0].cell, pbc=True)
data = [StandardCandidate.from_atoms(template, atoms) for atoms in data]

# Relax a few steps with the pre-fit model:
before_data = [atoms.copy() for atoms in data[0:4]]
before = relaxer.process_list(before_data)

# Train the model:
model.train(data)

# Again this happens automatically in a run, but do it manually here: 
relaxer.pool.update_modules(writer=print)

after_data = [atoms.copy() for atoms in data[0:4]]
after = relaxer.process_list(after_data)

before_energies = [atoms.get_potential_energy() for atoms in before]
after_energies = [atoms.get_potential_energy() for atoms in after]

print(f"{before_energies = }")
print(f"{after_energies = }")
```

The parallelization is pretty performant, here are timings for relaxing 10 structures 
50 steps, serially and in parallel. The number in paranthesis is the slowest serial 
relaxation, so the over-head is roughly ~4 seconds. 

```
# Relaxation timings: 
    Parallel time: 21.69 s
    Serial time: 137.53 s (17.81 s)
```





